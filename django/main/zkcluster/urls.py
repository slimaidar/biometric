from django.urls import re_path, include
from . import views

app_name = "zk"

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^i18n/', include('django.conf.urls.i18n')),
    re_path(r'^dashboard/$', views.dashboard, name='dashboard'),

    re_path(r'^terminal/$', views.terminal, name='terminal'),
    re_path(r'^terminal/add$', views.terminal_add, name='terminal_add'),
    re_path(r'^terminal/scan$', views.terminal_scan, name='terminal_scan'),
    re_path(r'^terminal/(?P<action>[\w-]+)/(?P<terminal_id>[0-9]+)/$', views.terminal_action, name='terminal_action'),

    re_path(r'^user/$', views.user, name='user'),
    re_path(r'^user/add/$', views.user_add, name='user_add'),
    re_path(r'^user/(?P<action>[\w-]+)/(?P<user_id>[0-9]+)/$', views.user_action, name='user_action'),

    re_path(r'^attendance/$', views.attendance, name='attendance'),
    re_path(r'^attendance/sync/$', views.attendance_sync, name='attendance_sync'),
]
