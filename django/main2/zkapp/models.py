from django.db import models

# Create your models here.
class Terminal(models.Model):
    name = models.CharField(max_length=200)
    serialnumber = models.CharField(max_length=100, unique=True)
    ip = models.CharField(max_length=15, unique=True)
    port = models.IntegerField(default=4370)
    # user = models.ForeignKeyField(User, on_delete=models.CASCADE)

    class Meta:
        db_table = 'zk_terminal'