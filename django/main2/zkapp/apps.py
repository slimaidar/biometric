from django.apps import AppConfig


class ZkappConfig(AppConfig):
    name = 'zkapp'
