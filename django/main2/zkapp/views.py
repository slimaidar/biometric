from django.shortcuts import render
from zk import ZK, const

# Create your views here.
def home(request):
    conn = None
    zk = ZK('192.168.1.201', port=4370, timeout=5)
    try:
        print ('Connecting to device ...')
        conn = zk.connect()
        print ('Disabling device ...')
        conn.disable_device()
        print ('Firmware Version: : {}'.format(conn.get_firmware_version()))
        # print '--- Get User ---'
        users = conn.get_users()
        for user in users:
            privilege = 'User'
            if user.privilege == const.USER_ADMIN:
                privilege = 'Admin'
        print ("Voice Test ...")
        conn.test_voice()
        print ('Enabling device ...')
        conn.enable_device()
    except Exception as e:
        print ("Process terminate : {}".format(e))

    return render(request, 'zkapp/home.html')
        